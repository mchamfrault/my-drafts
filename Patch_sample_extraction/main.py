import os
from time import time
import resource
from enum import Enum
import netCDF4
from FeaturesReader import FeaturesReader, FeatureReadMode

RADIUS_WIDTH = 5
RADIUS_HEIGHT = 5
RESULTS_PATH = '/home/MChamfrault/Documents/IOTA2_TEST_S2/IOTA2_Outputs (copie)/Results_classif/'

BAND_SIZE = 13
DATE_SIZE = 2

class SampleWriteMode(Enum):
    MEMORY = 1
    NETCDF = 2

class Monitor:
    def __init__(self):
        self.time0 = time()
        self.mem0 = resource.getrusage(resource.RUSAGE_SELF).ru_maxrss

    def print_usage(self, task: str):
        duration = time() - self.time0
        memory = (resource.getrusage(resource.RUSAGE_SELF).ru_maxrss - self.mem0) / 1000.0
        print(f"Processed {task} in {int(duration)} seconds using {int(memory)} MB of RAM")

def extract_features_by_patch(feature_read_mode: FeatureReadMode, sample_write_mode: SampleWriteMode):
    print(f"#### extractFeaturesByPatch {feature_read_mode} {sample_write_mode}")
    # Got the features file with a temporary dump in VectorSampler::get_features_application
    features_file = os.path.join(RESULTS_PATH, 'features/T31TCJ/tmp/T31TCJ_Features.tif')
    sample_selection_file = os.path.join(RESULTS_PATH, 'samplesSelection/T31TCJ_selection_merge.sqlite')
    monitor = Monitor()

    features_reader = FeaturesReader(features_file, sample_selection_file, feature_read_mode, RADIUS_WIDTH, RADIUS_HEIGHT)
    features_reader.open()

    if sample_write_mode is SampleWriteMode.NETCDF:
        rootgrp = netCDF4.Dataset("test.nc", "w", format="NETCDF4")
        rootgrp.createDimension("sample", None)
        rootgrp.createDimension("band", BAND_SIZE)
        rootgrp.createDimension("date", DATE_SIZE)
        rootgrp.createDimension("p_col", RADIUS_WIDTH * 2 + 1)
        rootgrp.createDimension("p_row", RADIUS_HEIGHT * 2 + 1)

        content = rootgrp.createVariable("content","f4",("sample","p_col","p_row","band","date"))

    #patches_features = {}
    nb_samples = 0
    for s_index, s_x, s_y in features_reader.get_samples():
        patch_bounds = features_reader.get_patch_bounds(s_index, s_x, s_y)
        if patch_bounds is False:
            continue
        nb_samples += 1
        if sample_write_mode is SampleWriteMode.MEMORY:
            patch_features = []
            for col, row, features in features_reader.get_features(patch_bounds):
                patch_features.append(col, row, features)
            #patches_features[s_index] = patch_features
        elif sample_write_mode is SampleWriteMode.NETCDF:
            for col, row, features in features_reader.get_features(patch_bounds):
                content[s_index, col, row, :, :] = features.reshape(BAND_SIZE, DATE_SIZE)

    monitor.print_usage(f"{nb_samples} samples")
    features_reader.close()

extract_features_by_patch(FeatureReadMode.ONE, SampleWriteMode.NETCDF)
