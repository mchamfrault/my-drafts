import os
import rasterio

class DebugInfo:
    def __init__(self, working_directory: str, srid: int,
                 raster_ref: rasterio.DatasetReader):
        import fiona
        schema = {'geometry': 'Polygon', 'properties': [('Id', 'str')]}

        def open_shape_file(file_path):
            return fiona.open(file_path,
                              mode='w',
                              driver='ESRI Shapefile',
                              schema=schema,
                              crs=f"EPSG:{srid}")

        samples_file_path = os.path.join(working_directory,
                                         f"SamplesWindow.shp")
        self.samples_shape_file = open_shape_file(samples_file_path)
        roi_file_path = os.path.join(working_directory, f"ROIWindow.shp")
        self.roi_shape_file = open_shape_file(roi_file_path)
        patch_file_path = os.path.join(working_directory, f"PatchWindow.shp")
        self.patch_shape_file = open_shape_file(patch_file_path)
        self.raster_ref = raster_ref

    def close(self):
        self.samples_shape_file.close()
        self.roi_shape_file.close()
        self.patch_shape_file.close()

    def shape_dic(self, id: str, x_left: float, x_right: float,
                  y_bottom: float, y_top: float):
        coordinates = [(x_left, y_bottom), (x_left, y_top), (x_right, y_top),
                       (x_right, y_bottom), (x_left, y_bottom)]
        return {
            'geometry': {
                'type': 'Polygon',
                'coordinates': [coordinates]
            },
            'properties': {
                'Id': f"{id}"
            }
        }

    def shape_dic_from_raster(self, id: str, row_min, row_max, col_min,
                              col_max):
        res_width, res_height = self.raster_ref.res
        x_left, y_top = self.raster_ref.xy(row_min, col_min)
        x_right, y_bottom = self.raster_ref.xy(row_max, col_max)
        return self.shape_dic(id, x_left - res_width / 2.,
                              x_right - res_width / 2.,
                              y_bottom + res_height / 2.,
                              y_top + res_height / 2.)

    def write_samples_window(self, roi_x: int, roi_y: int, x_left: float,
                             x_right: float, y_bottom: float, y_top: float):
        id = f"{roi_x}_{roi_y}"
        dic = self.shape_dic(id, x_left, x_right, y_bottom, y_top)
        self.samples_shape_file.write(dic)

    def write_roi_window(self, roi_x: int, roi_y: int, sizex: int, sizey: int):
        id = f"{roi_x}_{roi_y}"
        dic = self.shape_dic_from_raster(id, roi_y, roi_y + sizey, roi_x,
                                         roi_x + sizex)
        self.roi_shape_file.write(dic)

    def write_patch_window(self, id: str, row_min: int, row_max: int,
                           col_min: int, col_max: int, roi_y: int, roi_x: int):
        dic = self.shape_dic_from_raster(id, row_min + roi_y, row_max + roi_y,
                                         col_min + roi_x, col_max + roi_x)
        self.patch_shape_file.write(dic)