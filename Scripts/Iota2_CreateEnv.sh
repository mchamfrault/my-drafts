#!/bin/sh

. ./_Iota2_SetName.sh ${1}

conda create --name $NAME
conda activate $NAME
conda install python=3.9
conda install mamba
mamba install -c tardyb iota2_develop
conda deactivate
conda activate $NAME
Iota2.py -h
